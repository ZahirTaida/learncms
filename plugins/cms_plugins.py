from django.utils.translation import gettext_lazy as _
# from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Phone, Slide

@plugin_pool.register_plugin
class PhonePlugin(CMSPluginBase):
    model = Phone
    render_template = "phone_plugin.html"
    cache = False

@plugin_pool.register_plugin
class SlidePlugin(CMSPluginBase):
    model = Slide
    render_template = "slide_plugin.html"
    cache = False
