from cms.models.pluginmodel import CMSPlugin
from tinymce.models import HTMLField
from django.db import models

# Create your models here.
class Phone(CMSPlugin):
    name = models.CharField(max_length=100)
    price = models.DecimalField(decimal_places=0, max_digits=10)
    
    def __str__(self):
        return self.name

class Slide(CMSPlugin):
    title = models.CharField(max_length=255)
    information = models.CharField(max_length=255)
    description = HTMLField()
    active = models.BooleanField(default=False)
    def __str__(self):
        return self.title